package ua.pt.statisticsprocessing.stream;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.util.Arrays;

@Component
public class KafkaStreamProcess {

    String flight_json = "\\{.*\\}";
    String comma_split = "(?<=\\}),(?=\\{)";

    Pattern flight_pattern = Pattern.compile(flight_json);
    Pattern split_pattern = Pattern.compile(comma_split);

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    private KafkaStreams streams;

    public String getFlights(String data) {
        Matcher m = flight_pattern.matcher(data);
        m.find();
        return m.group();
    }

    @PostConstruct
    public void processStream() {

        Serde<String> stringSerde = Serdes.String();

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "planejane-statistics");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        StreamsBuilder builder = new StreamsBuilder();

        // fl.getIcao24()+","+fl.getEstDepartureAirport() +","+fl.getEstArrivalAirport()
        
        // true , false, false, false, ... , true

        KStream<String, String> route_stream = builder.stream("esp55_state_vector_routes",
                Consumed.with(stringSerde, stringSerde));
        
        KStream<String, String> route_count_stream = route_stream
                .map((k, v) -> KeyValue.pair(k, v.split(",")))
                .filter((k,v) -> !v[1].equals("null") &&  !v[2].equals("null") && !v[1].equals(v[2]))
                .map((k, v) -> {
                    String[] arr = new String[]{v[1], v[2]};
                    Arrays.sort(arr);
                    return KeyValue.pair(arr[0]+","+arr[1], v[0]);
                } )
                .groupByKey().count().toStream()
                .map((k,v) -> KeyValue.pair("", k+","+v));
                
        route_count_stream.to("esp55_route_count", Produced.with(stringSerde, stringSerde));

        streams = new KafkaStreams(builder.build(), props);
        streams.start();

    }

    @PreDestroy
    public void closeStream() {
        streams.close();
    }
}
