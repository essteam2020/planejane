package ua.pt.statisticsprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatisticsProcessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(StatisticsProcessingApplication.class, args);
	}

}
