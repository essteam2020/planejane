FROM adoptopenjdk/openjdk8-openj9:alpine-slim
VOLUME /tmp
COPY "target/*.jar" planejane.jar
ENTRYPOINT ["java","-jar","/planejane.jar"]