package ua.pt.planejaneaggregator.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ua.pt.planejaneaggregator.controller.PlaneJaneController;
import ua.pt.planejaneaggregator.model.Country;

@Service
public class NotificationService {

    HashMap<String, Boolean> planes_on_ground = new HashMap<>();
    HashMap<String, ArrayList<String>> nPlanes_by_region = new HashMap<>();
    HashMap<String, List<Country>> country_by_quadrant = new HashMap<String, List<Country>>() {
        {
            put("Q11", new ArrayList<Country>());
            put("Q12", new ArrayList<Country>());
            put("Q13", new ArrayList<Country>());
            put("Q21", new ArrayList<Country>());
            put("Q22", new ArrayList<Country>());
            put("Q23", new ArrayList<Country>());
        }
    };

    @Autowired
    PlaneJaneController controller;

    @Scheduled(fixedRate = 1000 * 60 * 10)
    public void sendData() {
        controller.planes_landed(nPlanes_by_region);
        nPlanes_by_region.clear();
    }

    // icao, onground, latitude, longitude
    public void addPlanePos(String payload) {
        String[] data = payload.split(",");
        Boolean on_ground = Boolean.parseBoolean(data[1]);

        double latitude = Double.parseDouble(data[2]);
        double longitude = Double.parseDouble(data[3]);

        if (planes_on_ground.containsKey(data[0])) {
            if (on_ground != planes_on_ground.get(data[0]) && on_ground) {
                int column = this.discoverColumn(longitude);
                int lin = this.discoverLine(longitude);
                String q = "Q" + lin + column;

                String min_country = "";
                double min_distance = Double.MAX_VALUE;

                for (Country ctr : country_by_quadrant.get(q)) {
                    double dist = Math.sqrt(
                            Math.pow(ctr.getLatitude() - latitude, 2) + Math.pow(ctr.getLongitude() - longitude, 2));
                    if (dist < min_distance) {
                        min_distance = dist;
                        min_country = ctr.getName();
                    }
                }
                if (!nPlanes_by_region.containsKey(min_country)) {
                    nPlanes_by_region.put(min_country, new ArrayList<String>());
                }
                nPlanes_by_region.get(min_country).add(data[0]);
            }
        }
        planes_on_ground.put(data[0], on_ground);
    }

    @PostConstruct
    public void readFile() {
        try {
            InputStream in = getClass().getResourceAsStream("/countries.csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                double lat = Double.parseDouble(data[0]);
                double longi = Double.parseDouble(data[1]);
                Country ctr = new Country(data[2], lat, longi);

                int column = this.discoverColumn(longi);
                int lin = this.discoverLine(lat);
                String q = "Q" + lin + column;
                country_by_quadrant.get(q).add(ctr);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int discoverColumn(double longi) {
        if (-180 < longi && longi < -60) {
            return 1;
        } else if (-60 < longi && longi < 60) {
            return 2;
        } else {
            return 3;
        }
    }

    private int discoverLine(double lat) {
        if (-90 < lat && lat < 0) {
            return 2;
        } else {
            return 1;
        }
    }
}
