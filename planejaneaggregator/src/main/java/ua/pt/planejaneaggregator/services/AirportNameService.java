package ua.pt.planejaneaggregator.services;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

@Service
public class AirportNameService {

    private HashMap<String, String> airport_names = new HashMap<>();

    @PostConstruct
    public void readFile() {
        try {
            InputStream in = getClass().getResourceAsStream("/airports_clean.csv"); 
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                this.airport_names.put(data[0], data[1]);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getName(String icao24) {
        return airport_names.get(icao24);
    }

}
