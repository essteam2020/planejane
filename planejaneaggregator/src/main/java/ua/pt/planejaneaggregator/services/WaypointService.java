package ua.pt.planejaneaggregator.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import ua.pt.planejaneaggregator.model.Waypoint;
import java.util.regex.Pattern;

@Service
public class WaypointService {
    private HashMap<String, List<Waypoint>> tracks = new HashMap<>();

	public void addTrack(String payload) {
        String[] data = payload.split(",",2);
        List<Waypoint> wpts = new ArrayList<>();
        for(String point : data[1].split(Pattern.quote("|"))){
            String[] coords = point.split(",");
            Waypoint wpt = new Waypoint(Float.parseFloat(coords[0]), Float.parseFloat(coords[1]));
            wpts.add(wpt);
        }
        tracks.put(data[0], wpts);
	}

    public List<Waypoint> getTrack(String icao24){
        return tracks.get(icao24);
    }


}