package ua.pt.planejaneaggregator.services;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class SpeedService {
    private HashMap<String, Float> icao_and_speed = new HashMap<String, Float>();

    public HashMap<String, Float> getIcao_and_speed() {
        return icao_and_speed;
    }

    public void setIcao_and_speed(HashMap<String, Float> icao_and_speed) {
        this.icao_and_speed = icao_and_speed;
    }

    public void addSpeed(String payload) {
        String[] data = payload.split(",");
        icao_and_speed.put(data[0], Float.parseFloat(data[1]));
    }

    public float getSpeed(String icao){
        return icao_and_speed.get(icao);
    }

}
