package ua.pt.planejaneaggregator.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pt.planejaneaggregator.model.Route;

@Service
public class RouteService {

    @Autowired
    AirportNameService ans;

    private ArrayList<Route> route_and_count = new ArrayList<Route>();

    public ArrayList<Route> getRoute_and_count() {
        return route_and_count;
    }

    public void setRoute_and_count(ArrayList<Route> route_and_count) {
        this.route_and_count = route_and_count;
    }

    public void addRoute(String payload) {
        String[] data = payload.split(",");
        Route rt = new Route(data[0], data[1], Integer.parseInt(data[2]));
        if (route_and_count.contains(rt)) {
            route_and_count.remove(rt);
        }
        rt.setArrivalAirportName(ans.getName(rt.getArrivalAirport()));
        rt.setDepartureAirportName(ans.getName(rt.getDepartureAirport()));
        route_and_count.add(rt);
        Collections.sort(route_and_count, new Comparator<Route>() {

            @Override
            public int compare(Route o1, Route o2) {
                return o2.getCount()-o1.getCount();
            }

        });
    }

    public int getCount(Route rt) {
        for (Route rtIns : route_and_count)
            if (rt.equals(rtIns)) {
                return rtIns.getCount();
            }
        return -1;
    }

    public List<Route> getCommonRoutes(int limit){
        return limit!=-1 ? route_and_count.stream().limit(limit).collect(Collectors.toList()) : route_and_count ;
    }

    

}
