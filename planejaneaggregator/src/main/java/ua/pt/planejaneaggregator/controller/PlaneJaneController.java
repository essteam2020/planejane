package ua.pt.planejaneaggregator.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.net.URISyntaxException;

import ua.pt.planejaneaggregator.model.Flight;
import ua.pt.planejaneaggregator.model.Route;
import ua.pt.planejaneaggregator.model.StateVector;
import ua.pt.planejaneaggregator.model.Waypoint;
import ua.pt.planejaneaggregator.services.AirportNameService;
import ua.pt.planejaneaggregator.services.RouteService;
import ua.pt.planejaneaggregator.services.SpeedService;
import ua.pt.planejaneaggregator.services.WaypointService;

@RestController
public class PlaneJaneController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RouteService route_service;

    @Autowired
    private SpeedService speed_service;

    @Autowired
    private WaypointService waypoint_service;

    @Autowired
    private AirportNameService airport_name_service;
   
    @Autowired
    private SimpMessagingTemplate webSocket;

    ObjectMapper omp = new ObjectMapper();

    /**
     * Allows to get information about all flights.
     * 
     * @return A list with the asked information.
     *
     */
    @GetMapping("/flights")
    public List<StateVector> flightsAll(
            @RequestParam(name = "limit", required = false, defaultValue = "") String limit) {

        URI rUrl = null;
        try {
            if (limit.equals(""))
                rUrl = new URI("http://192.168.160.20:55070/flights");
            else
                rUrl= new URI("http://192.168.160.20:55070/flights?limit="+limit);
        }catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Arrays.asList(invoke(rUrl, StateVector[].class));
    }

    // /flights/position?latitude=100.0&longitude=100.0
    @GetMapping("/flights/position")
    public List<StateVector> flightsPosition(
            @RequestParam(name = "latitude", required = false, defaultValue = "") String latitude,
            @RequestParam(name = "longitude", required = false, defaultValue = "") String longitude) {
      
        String urlString = "http://192.168.160.20:55070/flights/position";

        
        if(!latitude.equals("")){
            urlString += "?latitude=" + latitude;
            if(!longitude.equals("")){
                urlString += "&longitude=" + longitude; 
            }
        }else if(!longitude.equals("")){
            urlString += "?longitude=" + longitude; 
        }

        URI rUrl = null;
        try {
            rUrl = new URI(urlString);
        }catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return Arrays.asList(invoke(rUrl, StateVector[].class));
     
    }

    // /flights/time?startTime=100&finalTime=100123
    @GetMapping("/flights/time")
    public List<Flight> flightsTime(
            @RequestParam(name = "startTime", required = false, defaultValue = "") String startTime,
            @RequestParam(name = "finalTime", required = false, defaultValue = "") String finalTime) {
        
        String urlString = "http://192.168.160.20:55070/flights/time";

        if(!startTime.equals("")){
            urlString += "?startTime=" + startTime;
            if(!finalTime.equals("")){
                urlString += "&finalTime=" + finalTime; 
            }
        }else if(!finalTime.equals("")){
            urlString += "?finalTime=" + finalTime; 
        }

        URI rUrl = null;
        try {
            rUrl = new URI(urlString);
        }catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return Arrays.asList(invoke(rUrl, Flight[].class));
                
    }

    // /flight?flightId=100 este flightId tem de ser o icao24 do flight
    @GetMapping("/flight")
    public Flight getFlightById(@RequestParam(name = "flightId", required = false, defaultValue = "") String flightId) {
        URI rUrl = null;
        try {
            rUrl = new URI("http://192.168.160.20:55070/flight?flightId="+flightId);
        }catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return invoke(rUrl, Flight.class);
        
    }

    @GetMapping("/statistics/common-routes")
    public List<Route> getCommonRoutes(@RequestParam(name = "limit", required = false, defaultValue = "") String limit) {
        
        if(!limit.equals("")){
            return route_service.getCommonRoutes(Integer.parseInt(limit));
        }
        
        return route_service.getCommonRoutes(-1);

    }

    @GetMapping("/realtime/average-speed")
    public Float getAverageSpeed(@RequestParam(name = "icao24", required = true) String icao24) {
        return speed_service.getSpeed(icao24);
    }

    @GetMapping("/realtime/track")
    public List<Waypoint> getAirplaneTrack(@RequestParam(name = "icao24", required = true) String icao24) {
        return waypoint_service.getTrack(icao24);
    }


    private <T> T invoke(URI url, Class<T> responseType) {
        System.out.println("\n\nurl:"+url+"\n\n");
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<T> exchange = this.restTemplate
                .exchange(request, responseType);
        return exchange.getBody();
    }


    @GetMapping("/info/airport-names")
    public String getAirplaneName(@RequestParam(name = "icao24", required = true) String icao24) {
        return airport_name_service.getName(icao24);
    }

    public void planes_landed(HashMap<String, ArrayList<String>> planesByCountry) {
        String json="";
        try {
            json = omp.writeValueAsString(planesByCountry);
        } catch (Exception e) {
        }
        webSocket.convertAndSend("/landings", json);
    }

}