package ua.pt.planejaneaggregator.consumer;

import java.util.concurrent.CountDownLatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import ua.pt.planejaneaggregator.services.NotificationService;
import ua.pt.planejaneaggregator.services.RouteService;
import ua.pt.planejaneaggregator.services.SpeedService;
import ua.pt.planejaneaggregator.services.WaypointService;

public class KafkaConsumer {

    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private RouteService route_service;

    @Autowired
    private WaypointService waypoint_service;

    @Autowired
    private SpeedService speed_service;

    @Autowired
    private NotificationService notification_service;
    
    public CountDownLatch getLatch() {
        return latch;
    }

    @KafkaListener(topics = "esp55_flight_velocity_sum")
    public void receiveSpeed(String payload) {
        speed_service.addSpeed(payload);
        latch.countDown();
    }

    @KafkaListener(topics = "esp55_route_count")
    public void receiveCommonRoutes(String payload) {
        route_service.addRoute(payload);
        latch.countDown();
    }

    @KafkaListener(topics = "esp55_position_stream")
    public void receivePlaneTrack(String payload) {
        waypoint_service.addTrack(payload);
        latch.countDown();
    }

    @KafkaListener(topics = "esp55_state_vector_position")
    public void receivePlanePosition(String payload) {
        notification_service.addPlanePos(payload);
        latch.countDown();
    }
}