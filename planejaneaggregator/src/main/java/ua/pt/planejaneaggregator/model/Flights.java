package ua.pt.planejaneaggregator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
  * GET /flights/all
  * needs:
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/aircraft
  * needs:
  *  - icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation. All letters need to be lower case
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/arrival
  * needs:
  *  - airport	string	ICAO identier for the airport
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/departure
  * needs:
  *  - airport	string	ICAO identier for the airport
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */
  
@JsonIgnoreProperties(ignoreUnknown = true)
public class Flights implements Serializable {

    private Long id;

    public List<Flight> flights = new ArrayList<Flight>();

    public Flights(){}

    public Flights(List<Flight> flights){
        this.flights = flights;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

}
