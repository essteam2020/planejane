package ua.pt.planejaneaggregator.model;

public class Route {

    private String arrivalAirport;
    private String departureAirport;
    private int count;
    private String arrivalAirportName;
    private String departureAirportName;

    public Route(String arrivalAirport, String departureAirport, int count) {
        this.arrivalAirport = arrivalAirport;
        this.departureAirport = departureAirport;
        this.count = count;
    }

    public Route(String arrivalAirport, String departureAirport) {
        this.arrivalAirport = arrivalAirport;
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
        result = prime * result + ((departureAirport == null) ? 0 : departureAirport.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Route other = (Route) obj;
        if (arrivalAirport == null) {
            if (other.arrivalAirport != null)
                return false;
        } else if (!arrivalAirport.equals(other.arrivalAirport))
            return false;
        if (departureAirport == null) {
            if (other.departureAirport != null)
                return false;
        } else if (!departureAirport.equals(other.departureAirport))
            return false;
        return true;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }


    
}