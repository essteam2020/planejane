package ua.pt.planejaneaggregator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/*
* GET /states/all
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllStates implements Serializable {


    private Long id;

    private int time;
    
    private List<StateVector> states = new ArrayList<StateVector>();

    public AllStates() {
    }

    public AllStates(int time, List<StateVector> states){
        this.time = time;
        this.states = states;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<StateVector> getStates() {
        return states;
    }

    public void setStates(List<StateVector> states) {
        this.states = states;
    }

}
