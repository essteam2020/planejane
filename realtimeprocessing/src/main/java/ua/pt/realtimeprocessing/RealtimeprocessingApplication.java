package ua.pt.realtimeprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class RealtimeprocessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealtimeprocessingApplication.class, args);
	}

}
