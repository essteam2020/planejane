package ua.pt.realtimeprocessing.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class StateVector implements Serializable{

    private Long id;
    private String icao24;
    private String callsign;
    private String origin_country;
    private int time_position;
    private int last_contact;
    private float longitude;
    private float latitude;
    private float baro_altitude;
    private boolean on_ground;
    private float velocity;
    private float true_track;
    private float vertical_rate;
    private float geo_altitude;
    private String squawk;
    private boolean spi;
    private int position_source;
    
    public StateVector() {

    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getOrigin_country() {
        return origin_country;
    }

    public void setOrigin_country(String origin_country) {
        this.origin_country = origin_country;
    }

    public int getTime_position() {
        return time_position;
    }

    public void setTime_position(int time_position) {
        this.time_position = time_position;
    }

    public int getLast_contact() {
        return last_contact;
    }

    public void setLast_contact(int last_contact) {
        this.last_contact = last_contact;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getBaro_altitude() {
        return baro_altitude;
    }

    public void setBaro_altitude(float baro_altitude) {
        this.baro_altitude = baro_altitude;
    }

    public boolean isOn_ground() {
        return on_ground;
    }

    public void setOn_ground(boolean on_ground) {
        this.on_ground = on_ground;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public float getTrue_track() {
        return true_track;
    }

    public void setTrue_track(float true_track) {
        this.true_track = true_track;
    }

    public float getVertical_rate() {
        return vertical_rate;
    }

    public void setVertical_rate(float vertical_rate) {
        this.vertical_rate = vertical_rate;
    }


    public float getGeo_altitude() {
        return geo_altitude;
    }

    public void setGeo_altitude(float geo_altitude) {
        this.geo_altitude = geo_altitude;
    }

    public String getSquawk() {
        return squawk;
    }

    public void setSquawk(String squawk) {
        this.squawk = squawk;
    }

    public boolean isSpi() {
        return spi;
    }

    public void setSpi(boolean spi) {
        this.spi = spi;
    }

    public int getPosition_source() {
        return position_source;
    }

    public void setPosition_source(int position_source) {
        this.position_source = position_source;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        StateVector other = (StateVector) obj;
        if (Float.floatToIntBits(baro_altitude) != Float.floatToIntBits(other.baro_altitude))
            return false;
        if (callsign == null) {
            if (other.callsign != null)
                return false;
        } else if (!callsign.equals(other.callsign))
            return false;
        if (Float.floatToIntBits(geo_altitude) != Float.floatToIntBits(other.geo_altitude))
            return false;
        if (icao24 == null) {
            if (other.icao24 != null)
                return false;
        } else if (!icao24.equals(other.icao24))
            return false;
        if (last_contact != other.last_contact)
            return false;
        if (Float.floatToIntBits(latitude) != Float.floatToIntBits(other.latitude))
            return false;
        if (Float.floatToIntBits(longitude) != Float.floatToIntBits(other.longitude))
            return false;
        if (on_ground != other.on_ground)
            return false;
        if (origin_country == null) {
            if (other.origin_country != null)
                return false;
        } else if (!origin_country.equals(other.origin_country))
            return false;
        if (position_source != other.position_source)
            return false;
        if (spi != other.spi)
            return false;
        if (squawk == null) {
            if (other.squawk != null)
                return false;
        } else if (!squawk.equals(other.squawk))
            return false;
        if (time_position != other.time_position)
            return false;
        if (Float.floatToIntBits(true_track) != Float.floatToIntBits(other.true_track))
            return false;
        if (Float.floatToIntBits(velocity) != Float.floatToIntBits(other.velocity))
            return false;
        if (Float.floatToIntBits(vertical_rate) != Float.floatToIntBits(other.vertical_rate))
            return false;
        return true;
    }

}