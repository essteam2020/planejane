package ua.pt.realtimeprocessing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/*
* GET /states/all
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllStates implements Serializable {


    private Long id;
    private int time;
    private List<StateVector> states = new ArrayList<StateVector>();

    public AllStates() {
    }

    public AllStates(int time, List<StateVector> states){
        this.time = time;
        this.states = states;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<StateVector> getStates() {
        return states;
    }

    public void setStates(List<StateVector> states) {
        this.states = states;
    }

}

/*
 * OpenSky users can retrieve data of up to 1 hour in the past. If the time
 * parameter has a value t<now−3600 the API will return 400 Bad Request. OpenSky
 * users can retrieve data with a time resultion of 5 seconds. That means, if
 * the time parameter was set to t, the API will return state vectors for time
 * t−(t mod 5).
 */

/*
 * time integer The time which the state vectors in this response are associated
 * with. All vectors represent the state of a vehicle with the interval
 * [time−1,time]. states array The state vectors.
 */