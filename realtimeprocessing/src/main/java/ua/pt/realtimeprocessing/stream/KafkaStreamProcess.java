package ua.pt.realtimeprocessing.stream;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KafkaStreamProcess {

    String flight_json = "\\{.*\\}";
    String comma_split = "(?<=\\}),(?=\\{)";

    Pattern flight_pattern = Pattern.compile(flight_json);
    Pattern split_pattern = Pattern.compile(comma_split);

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    private KafkaStreams streams;

    public String getFlights(String data) {
        Matcher m = flight_pattern.matcher(data);
        m.find();
        return m.group();
    }

    @PostConstruct
    public void processStream() {

        Serde<String> stringSerde = Serdes.String();

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "planejane-realtime-processor");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        StreamsBuilder builder = new StreamsBuilder();

        // stream to get the average velocity
        KStream<String, String> velocity_stream = builder.stream("esp55_state_vector_velocity",
                Consumed.with(stringSerde, stringSerde));

        KStream<String, String> velocity_count_stream = velocity_stream.map((k, v) -> KeyValue.pair(k, v.split(",")))
                .map((k, v) -> KeyValue.pair(v[0], "1," + v[1])).groupByKey().reduce(new Reducer<String>() {
                    @Override
                    public String apply(String previousF, String currentF) {
                        String[] previous = previousF.split(",");
                        String[] current = currentF.split(",");

                        float prevValue = Float.parseFloat(previous[1]);
                        float currValue = Float.parseFloat(current[1]);

                        float prevCount = Float.parseFloat(previous[0]);
                        float currCount = Float.parseFloat(current[0]);

                        return currCount + prevCount + "," + (prevValue + currValue);
                    }
                }).toStream();

        velocity_count_stream.map((k, v) -> {
            String[] val = v.split(",");
            float sum = Float.parseFloat(val[1]);
            float count = Float.parseFloat(val[0]);
            float avg_speed = sum / count;
            return KeyValue.pair("", k + "," + avg_speed);
        }).to("esp55_flight_velocity_sum", Produced.with(stringSerde, stringSerde));



        // Plane position stream - input: icao,on_ground,latitude,longitude
        // icao1,icao2, 1
        KStream<String, String> position_stream = builder.stream("esp55_state_vector_position", Consumed.with(stringSerde, stringSerde))
                .map((k,v) -> KeyValue.pair(k, v.split(",")))
                .map((k,v) -> KeyValue.pair(v[0], v[1] + "," + v[2] + "," + v[3]))
                .groupByKey().reduce(new Reducer<String>() {
                    @Override
                    public String apply(String previous, String current) {
                        String[] prev = previous.split(",");
                        String[] curr = current.split(",");

                        System.out.println("prev: " + previous);
                        System.out.println("curr: " + current);

                        if(!previous.isEmpty())
                            try {
                                float f = Float.parseFloat(prev[0]);
                            } catch (Exception e) {
                                previous = prev[1] + "," + prev[2] + "|";
                            }

                        if(Boolean.parseBoolean(curr[0])){
                            previous = curr[1] +","+curr[2] + "|";
                        }  
                        else{
                            previous += curr[1] +","+curr[2] + "|";
                        }
                                                
                        return previous;
                    }
                }).toStream()
                .filter((k,v) -> v.split(",").length > 1)
                .map((k,v) ->  {
                    String[] data = v.split(",");
                    try {
                        float f = Float.parseFloat(data[0]);
                        return KeyValue.pair("", k + "," + v);

                    } catch (Exception e) {
                        return KeyValue.pair("", k + "," + data[1]+"," + data[2]);
                    }
                });

        // a09a01,38.0979,-121.9657, 38.0996

        // a09a01,38.0979,-121.965738.0996|38.0238,-121.9137|38.0238,-121.9137|38.0238,-121.9137|37.9893,-122.0573|38.0238,-121.9137|37.9893,-122.0573|38.0238,-121.9137|37.9893,-122.0573|37.9851,-122.0596|
        position_stream.to("esp55_position_stream", Produced.with(stringSerde, stringSerde));

        streams = new KafkaStreams(builder.build(), props);
        streams.start();

    }

    @PreDestroy
    public void closeStream() {
        streams.close();
    }
}

