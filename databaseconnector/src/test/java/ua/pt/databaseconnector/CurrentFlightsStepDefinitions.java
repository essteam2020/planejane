package ua.pt.databaseconnector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;

import static org.hamcrest.Matchers.*;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ua.pt.databaseconnector.model.AllStates;
import ua.pt.databaseconnector.model.Flight;
import ua.pt.databaseconnector.model.StateVector;
import ua.pt.databaseconnector.repositories.AllStateRepository;
import ua.pt.databaseconnector.repositories.FlightRepository;
import ua.pt.databaseconnector.repositories.StateVectorRepository;

public class CurrentFlightsStepDefinitions extends SpringIntegrationTest {

    @Autowired
    private AllStateRepository asRep;

    @Autowired
    private FlightRepository fRep;

    @Before
    public void setUp() {
        StateVector sv1 = new StateVector();
        StateVector sv2 = new StateVector();
        ArrayList<StateVector> sv_list = new ArrayList<StateVector>();
        
        sv1.setIcao24("ABCDE");
        sv1.setOrigin_country("PT");
        sv1.setLongitude(0.0f);
        sv1.setLatitude(0.0f);
        sv1.setOn_ground(true);
        sv1.setVelocity(0);
        
        sv2.setIcao24("AEDES");
        sv2.setOrigin_country("PT");
        sv2.setLongitude(10.0f);
        sv2.setLatitude(-10.0f);
        sv2.setOn_ground(false);
        sv2.setVelocity(1000.0f);

        sv_list.add(sv1);
        sv_list.add(sv2);
        
        AllStates all_states = new AllStates(1000, sv_list);
    
        Mockito.when(asRep.findTopByOrderByIdDesc()).thenReturn(all_states);
        
        
        Flight flight = new Flight();
        flight.setIcao24("ABCDE");
        flight.setFirstSeen(152412381);
        flight.setLastSeen(152412384);
        flight.setEstDepartureAirport("Cristiano Ronaldo");
        flight.setEstArrivalAirport("Sá Carneiro");
        flight.setCallSign("1a2a4h3");
        flight.setEstDepartureAirportHorizDistance(1200);
        flight.setEstDepartureAirportVertDistance(1200);
        flight.setEstArrivalAirportHorizDistance(1200);
        flight.setEstArrivalAirportVertDistance(1200);
        flight.setDepartureAirportCandidatesCount(2);
        flight.setArrivalAirportCandidatesCount(2);

        Flight flight2 = new Flight();
        flight2.setIcao24("AEDES");
        flight2.setFirstSeen(152412384);
        flight2.setLastSeen(152412394);
        flight2.setEstDepartureAirport("Cristiano Ronaldo");
        flight2.setEstArrivalAirport("Sá Carneiro");
        flight2.setCallSign("1a2a4h3");
        flight2.setEstDepartureAirportHorizDistance(1200);
        flight2.setEstDepartureAirportVertDistance(1200);
        flight2.setEstArrivalAirportHorizDistance(1200);
        flight2.setEstArrivalAirportVertDistance(1200);
        flight2.setDepartureAirportCandidatesCount(2);
        flight2.setArrivalAirportCandidatesCount(2);
        
        List<Flight> flights = new ArrayList<>();
        flights.add(flight);
        Mockito.when(fRep.findFirst(PageRequest.of(0,1))).thenReturn(flights);
        

        flights.add(flight2);
        Mockito.when(fRep.findByFirstSeenGreaterThanAndLastSeenLessThan(152412381, 152414444)).thenReturn(flights);

        Mockito.when(fRep.findTopByIcao24OrderByIdDesc("ABCDE")).thenReturn(flight);
    }

    ObjectMapper mapper = new ObjectMapper();

    @When("the client calls \\/flights")
    public void the_client_calls_flights() throws IOException {
        executeGet("http://localhost:55070/flights");
    }

    @Then("the client receives a json file containing a StateVector list with the most recent flights")
    public void the_client_receives_a_json_file_containing_a_StateVector_list_with_the_most_recent_flights()
            throws JsonMappingException, JsonProcessingException {
        AllStates as = asRep.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();

        String body = latestResponse.getBody();
        List<StateVector> fls = Arrays.asList(mapper.readValue(body, StateVector[].class));
        
        assertThat(as_vectors.size(), is(fls.size()));
        assertTrue(as_vectors.equals(fls));
    }


    @When("the client calls \\/flights with limit value {int}")
    public void the_client_calls_flights_with_limit_value(Integer int1) throws IOException {
        executeGet("http://localhost:55070/flights?limit=" + int1);
    }
    @Then("the client receives a json file containing a StateVector list with the {int} most recent flights")
    public void the_client_receives_a_json_file_containing_a_StateVector_list_with_the_most_recent_flights(Integer limit)
            throws JsonMappingException, JsonProcessingException {
        
        AllStates as = asRep.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();  
        int lim = limit < as_vectors.size() ? limit : as_vectors.size();
        for (int i = 0; i < lim; i++) {
            ret.add(as_vectors.get(i));
        }
        
        String body = latestResponse.getBody();
        List<StateVector> fls = Arrays.asList(mapper.readValue(body, StateVector[].class));
        
        assertThat(ret.size(), is(fls.size()));
        assertTrue(ret.equals(fls));
    }


    @When("the client calls \\/flights\\/position with latitude equals to {double}")
    public void the_client_calls_flights_position_with_latitude_equals_to(Double double1) throws IOException {
        executeGet("http://localhost:55070/flights/position?latitude=" + double1);
    }
    @Then("the client receives a json file containing a StateVector list with flights with latitude values near {double}")
    public void the_client_receives_a_json_file_containing_a_StateVector_list_with_flights_with_latitude_values_near(Double double1)
            throws JsonMappingException, JsonProcessingException {
        AllStates as = asRep.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();

        for (StateVector st : as_vectors) 
            if (st.getLatitude() > double1 - 0.05 && st.getLatitude() < double1 + 0.05) {
                ret.add(st);
            }
        String body = latestResponse.getBody();
        List<StateVector> fls = Arrays.asList(mapper.readValue(body, StateVector[].class));
        
        assertThat(ret.size(), is(fls.size()));
        assertTrue(ret.equals(fls));

    }


    @When("the client calls \\/flights\\/position with longitude equals to {double}")
    public void the_client_calls_flights_position_with_longitude_equals_to(Double double1) throws IOException {
        executeGet("http://localhost:55070/flights/position?longitude=" + double1);
    }
    @Then("the client receives a json file containing a StateVector list with flights with longitude values near {double}")
    public void the_client_receives_a_json_file_containing_a_StateVector_list_with_flights_with_longitude_values_near(Double double1) throws JsonMappingException, JsonProcessingException {
        AllStates as = asRep.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();

        for (StateVector st : as_vectors) 
            if (st.getLongitude() > (double1 - 0.05) && st.getLongitude() < (double1 + 0.05)) {
                ret.add(st);
            }
        String body = latestResponse.getBody();
        List<StateVector> fls = Arrays.asList(mapper.readValue(body, StateVector[].class));
        
        assertThat(ret.size(), is(fls.size()));
        assertTrue(ret.equals(fls));
    }


    @When("the client calls \\/flights\\/position with latitude equals to {double} and longitude equals to {double}")
    public void the_client_calls_flights_position_with_latitude_equals_to_and_longitude_equals_to(Double double1, Double double2) throws IOException{
        executeGet("http://localhost:55070/flights/position?latitude=" + double1 + "&longitude=" + double2);
    }
    @Then("the client receives a json file containing a StateVector list with flights with latitude values near {double} and longitude values near {double}")
    public void the_client_receives_a_json_file_containing_a_StateVector_list_with_flights_with_latitude_values_near_and_longitude_values_near(Double double1, Double double2) throws JsonMappingException, JsonProcessingException{
        AllStates as = asRep.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();

        for (StateVector st : as_vectors) {
            if ((st.getLongitude() > double2 - 0.05 && st.getLongitude() < double2 + 0.05)
                    && (st.getLatitude() > double1 - 0.05 && st.getLatitude() < double1 + 0.05)) {
                ret.add(st);
            }
        }

        String body = latestResponse.getBody();
        List<StateVector> fls = Arrays.asList(mapper.readValue(body, StateVector[].class));
        
        assertThat(ret.size(), is(fls.size()));
        assertTrue(ret.equals(fls));
    }
    

    @When("the client send calls \\/flight with id")
    public void the_client_send_calls_flight_with_id() throws IOException {

        Flight flight = fRep.findFirst(PageRequest.of(0,1)).get(0);

        executeGet("http://localhost:55070/flight?flightId=" + flight.getIcao24());

    }
    @Then("the client receives a json file containing a Flight")
    public void the_client_receives_a_json_file_containing_a_Flight()
            throws JsonMappingException, JsonProcessingException {
        
        Flight f = fRep.findFirst(PageRequest.of(0,1)).get(0);

        String body = latestResponse.getBody();
        Flight fr = mapper.readValue(body, Flight.class);

        assertEquals(f, fr);
    }
    

    @When("the client calls \\/flights\\/time with startTime {int} and finalTime {int}")
    public void the_client_calls_flights_time_with_startTime_and_finalTime(Integer int1, Integer int2)
            throws IOException {
        executeGet("http://localhost:55070/flights/time?startTime=" + int1 + "&finalTime=" + int2);
    }
    @Then("the client receives a json file containing a Flight list with flights that started after {int} and ended before {int}")
    public void the_client_receives_a_json_file_containing_a_Flight_list_with_flights_that_started_after_and_ended_before(Integer int1, Integer int2)
            throws JsonMappingException, JsonProcessingException {

        List<Flight> flights = fRep.findByFirstSeenGreaterThanAndLastSeenLessThan(int1, int2);
        // flights = flights.stream().filter(flight -> flight.getFirstSeen() > int1 && flight.getLastSeen() < int2)
        //         .collect(Collectors.toList());

        String body = latestResponse.getBody();
        List<Flight> fls = Arrays.asList(mapper.readValue(body, Flight[].class));
        
        assertThat(flights.size(), is(fls.size()));
        assertTrue(flights.equals(fls));
    }
    

    @When("the client calls \\/flights\\/time with startTime {int}")
    public void the_client_calls_flights_time_with_startTime(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Then("the client receives a json file containing a Flight list with flights that started after {int}")
    public void the_client_receives_a_json_file_containing_a_Flight_list_with_flights_that_started_after(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    
    
    @When("the client calls \\/flights\\/time with finalTime {int}")
    public void the_client_calls_flights_time_with_finalTime(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Then("the client receives a json file containing a Flight list with flights that ended before {int}")
    public void the_client_receives_a_json_file_containing_a_Flight_list_with_flights_that_ended_before(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }


    @Then("the client receives status code of {int}")
    public void the_client_receives_status_code_of(Integer statusCode) throws IOException {
        final HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();
        assertThat("status code is incorrect : " + latestResponse.getBody(), currentStatusCode.value(), is(statusCode));
    }

}





