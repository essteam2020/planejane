package ua.pt.databaseconnector;

import org.springframework.web.client.RestTemplate;

import ua.pt.databaseconnector.repositories.AllStateRepository;
import ua.pt.databaseconnector.repositories.FlightRepository;
import ua.pt.databaseconnector.repositories.StateVectorRepository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResponseErrorHandler;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatabaseConnectorApplication.class, properties = {
        "scheduled.chron=0 0 0 29 2 ?", "spring.main.allow-bean-definition-overriding=true"}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class SpringIntegrationTest {

    @MockBean
    private StateVectorRepository svRep;

    @MockBean
    private AllStateRepository asRep;

    @MockBean
    private FlightRepository fRep;

    protected RestTemplate restTemplate = new RestTemplate();

    static ResponseResults latestResponse = null;

    void executeGet(String url) throws IOException {
        final Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        final HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(headers);
        final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate.execute(url, HttpMethod.GET, requestCallback, response -> {
            if (errorHandler.hadError) {
                return (errorHandler.getResults());
            } else {
                return (new ResponseResults(response));
            }
        });
    }

    private class ResponseResultErrorHandler implements ResponseErrorHandler {
        private ResponseResults results = null;
        private Boolean hadError = false;

        private ResponseResults getResults() {
            return results;
        }

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            hadError = response.getRawStatusCode() >= 400;
            return hadError;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
            results = new ResponseResults(response);
        }
    }

}
