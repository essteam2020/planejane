Feature: a flight with a certain icao24 can be retrieved

  Scenario: client makes call to GET flight by id
    When the client send calls /flight with id
    Then the client receives status code of 200
    And the client receives a json file containing a Flight
