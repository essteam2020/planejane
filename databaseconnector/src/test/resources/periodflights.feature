Feature: the flights that have flown during a certain period can be retrieved

  Scenario: client makes call to GET /flights/time with startTime 1587491623 and finalTime 1587495223
    When the client calls /flights/time with startTime 1587491623 and finalTime 1587495223
    Then the client receives status code of 200
    And the client receives a json file containing a Flight list with flights that started after 1587491623 and ended before 1587495223

  Scenario: client makes call to GET /flights/time with startTime 100
    When the client calls /flights/time with startTime 100
    Then the client receives status code of 200
    And the client receives a json file containing a Flight list with flights that started after 100

  Scenario: client makes call to GET /flights/time with finalTime 100123
    When the client calls /flights/time with finalTime 100123
    Then the client receives status code of 200
    And the client receives a json file containing a Flight list with flights that ended before 100123
