Feature: the current flights can be retrieved

  Scenario: client makes call to GET /flights
    When the client calls /flights
    Then the client receives status code of 200
    And the client receives a json file containing a StateVector list with the most recent flights

  Scenario: client makes call to GET /flights with limit value 100
    When the client calls /flights with limit value 100
    Then the client receives status code of 200
    And the client receives a json file containing a StateVector list with the 100 most recent flights

  Scenario: client makes call to GET /flights/position with latitude equals to 29.75281
    When the client calls /flights/position with latitude equals to 29.75281
    Then the client receives status code of 200
    And the client receives a json file containing a StateVector list with flights with latitude values near 29.75281

  Scenario: client makes call to GET /flights/position with longitude equals to -81.96326
    When the client calls /flights/position with longitude equals to -81.96326
    Then the client receives status code of 200
    And the client receives a json file containing a StateVector list with flights with longitude values near -81.96326

  Scenario: client makes call to GET /flights/position with latitude equals to 29.75281 and longitude equals to -81.96326
    When the client calls /flights/position with latitude equals to 29.75281 and longitude equals to -81.96326
    Then the client receives status code of 200
    And the client receives a json file containing a StateVector list with flights with latitude values near 29.75281 and longitude values near -81.96326
