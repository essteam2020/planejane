package ua.pt.databaseconnector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pt.databaseconnector.model.StateVector;

/**
 * StateVectorRepository
 */
public interface StateVectorRepository extends JpaRepository<StateVector, Long>{
    // List<StateVector> findByAllStates(AllStates allStates);
}