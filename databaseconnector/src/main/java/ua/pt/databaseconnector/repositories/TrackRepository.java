package ua.pt.databaseconnector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.pt.databaseconnector.model.Track;

/**
 * TrackRepository
 */
public interface TrackRepository extends JpaRepository<Track, Long>{
        
}