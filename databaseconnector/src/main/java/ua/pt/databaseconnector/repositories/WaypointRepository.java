package ua.pt.databaseconnector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.pt.databaseconnector.model.Waypoint;

/**
 * WaypointRepository
 */
public interface WaypointRepository extends JpaRepository<Waypoint, Long>{
        
}