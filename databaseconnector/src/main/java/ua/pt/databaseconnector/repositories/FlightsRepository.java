package ua.pt.databaseconnector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.pt.databaseconnector.model.Flights;

/**
 * FlightsRepository
 */
public interface FlightsRepository extends JpaRepository<Flights, Long>{
        
}