package ua.pt.databaseconnector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.pt.databaseconnector.model.AllStates;

/**
 * AllStateRepository
 */
public interface AllStateRepository extends JpaRepository<AllStates, Long>{
    AllStates findTopByOrderByIdDesc();
}