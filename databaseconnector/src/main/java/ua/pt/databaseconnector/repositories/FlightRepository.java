package ua.pt.databaseconnector.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ua.pt.databaseconnector.model.Flight;

/**
 * FlightRepository
 */
public interface FlightRepository extends JpaRepository<Flight, Long>{
    Flight findTopByIcao24OrderByIdDesc(String icao24);
    
    @Query(value = "SELECT u FROM Flight u")
    List<Flight> findFirst(Pageable limit);

    List<Flight> findByFirstSeenGreaterThanAndLastSeenLessThan(int firstseen, int lastseen);

}