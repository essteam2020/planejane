package ua.pt.databaseconnector.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import ua.pt.databaseconnector.model.AllStates;
import ua.pt.databaseconnector.model.Flight;
import ua.pt.databaseconnector.model.StateVector;
import ua.pt.databaseconnector.repositories.AllStateRepository;
import ua.pt.databaseconnector.repositories.FlightRepository;

@RestController
public class PlaneJaneController {

    @Autowired
    private AllStateRepository asr;

    @Autowired
    private FlightRepository fr;

    @GetMapping("/")
    public String index(Model model) {
        // AllStates as = asr.findAll(Sort.by(Sort.Direction.DESC, "time")).get(0);
        // model.addAttribute("states", as.getStates());
        return "index";
    }

    /**
     * Allows to get information about all flights.
     * 
     * @return A list with the asked information.
     *
     */
    @GetMapping("/flights")
    public List<StateVector> flightsAll(
            @RequestParam(name = "limit", required = false, defaultValue = "") String limit) {
        
        AllStates as = asr.findTopByOrderByIdDesc();
        
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();
        if (!limit.equals("")) {
            int lim = Integer.parseInt(limit) < as_vectors.size() ? Integer.parseInt(limit) : as_vectors.size();
            for (int i = 0; i < lim; i++) {
                ret.add(as_vectors.get(i));
            }
            return ret;
        }
        return as_vectors;
    }

    // /flights/position?latitude=100.0&longitude=100.0
    @GetMapping("/flights/position")
    public List<StateVector> flightsPosition(
            @RequestParam(name = "latitude", required = false, defaultValue = "") String latitude,
            @RequestParam(name = "longitude", required = false, defaultValue = "") String longitude) {
        AllStates as = asr.findTopByOrderByIdDesc();
        List<StateVector> as_vectors = as.getStates();
        List<StateVector> ret = new ArrayList<>();
        if (!latitude.equals("")) {
            float lat = Float.parseFloat(latitude);
            if (!longitude.equals("")) {
                float longi = Float.parseFloat(longitude);
                for (StateVector st : as_vectors) {
                    if ((st.getLongitude() > longi - 0.05 && st.getLongitude() < longi + 0.05)
                            && (st.getLatitude() > lat - 0.05 && st.getLatitude() < lat + 0.05)) {
                        ret.add(st);
                    }
                }
            } else {
                for (StateVector st : as_vectors) {
                    if (st.getLatitude() > lat - 0.05 && st.getLatitude() < lat + 0.05) {
                        ret.add(st);
                    }
                }
            }

        } else if (!longitude.equals("")) {
            float longi = Float.parseFloat(longitude);
            for (StateVector st : as_vectors) {
                if (st.getLongitude() > longi - 0.05 && st.getLongitude() < longi + 0.05) {
                    ret.add(st);
                }
            }
        }

        return ret;
    }

    // @GetMapping("/flights/relevant")
    // public List<Flight> flightsRelevant() {
    //     return null;
    // }

    // /flights/time?startTime=100&finalTime=100123
    @GetMapping("/flights/time")
    public List<Flight> flightsTime(
            @RequestParam(name = "startTime", required = false, defaultValue = "") String startTime,
            @RequestParam(name = "finalTime", required = false, defaultValue = "") String finalTime) {
        
        int start = Integer.parseInt(startTime);
        int finl = Integer.parseInt(finalTime);
        List<Flight> flights = fr.findByFirstSeenGreaterThanAndLastSeenLessThan(start, finl);
        // flights = flights.stream().filter(flight -> flight.getFirstSeen() > start && flight.getLastSeen() < finl)
        //         .collect(Collectors.toList());
        return flights;
    }

    // /flight?flightId=100 este flightId tem de ser o icao24 do flight
    @GetMapping("/flight")
    public Flight getFlightById(@RequestParam(name = "flightId", required = false, defaultValue = "") String flightId) {
        return fr.findTopByIcao24OrderByIdDesc(flightId);
    }

}