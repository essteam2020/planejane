package ua.pt.databaseconnector.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ua.pt.databaseconnector.model.*;
import ua.pt.databaseconnector.producer.KafkaProducer;
import ua.pt.databaseconnector.repositories.AllStateRepository;
import ua.pt.databaseconnector.repositories.FlightsRepository;
import ua.pt.databaseconnector.service.PlaneJaneService;

/**
 * ScheduleRetrieve
 */
@Component
public class ScheduleRetrieve {

    @Autowired
    PlaneJaneService pjs;

    @Autowired
    AllStateRepository asr;

    @Autowired
    FlightsRepository fsr;

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;
    
    @Autowired
    KafkaProducer sender;

    ObjectMapper omp = new ObjectMapper();

    private static final Logger logger = LoggerFactory.getLogger(ScheduleRetrieve.class);


    @Scheduled(cron = "${scheduled.chron}")
    public void getAllStates() {
        
        AllStates als = pjs.getAllStates();
        asr.save(als);
        for(StateVector sv : als.getStates()){
            try {
                sender.send("esp55_state_vector_velocity", sv.getIcao24() + "," + sv.getVelocity());

                sender.send("esp55_state_vector_position", sv.getIcao24() + "," + sv.isOn_ground() + "," + sv.getLatitude() + "," + sv.getLongitude());

                logger.info("StateVectorPosition-" + sv.getIcao24() + " " +sv.getLatitude() +" "+ sv.getLongitude() );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("SCHEDULELOG- I retrieved "+ als.getStates().size() + " state_vectors!");

    }

    @Scheduled(cron = "${scheduled.chron}")
    public void getAllFlights() {
        long currTime = (System.currentTimeMillis() / 1000L) - 86400L;
        long prevTime = currTime - 3600L;
        Flights als = pjs.getFlightsWithTime(prevTime, currTime);
        fsr.save(als);
        for(Flight fl : als.getFlights()){
            sender.send("esp55_state_vector_routes", fl.getIcao24()+","+fl.getEstDepartureAirport() +","+fl.getEstArrivalAirport());
        }
        logger.info("SCHEDULELOG- I retrieved "+ als.getFlights().size() + " flights from yesterday!");
    }

}