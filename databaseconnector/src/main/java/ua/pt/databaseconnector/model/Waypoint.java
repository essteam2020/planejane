package ua.pt.databaseconnector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "waypoint")
@EntityListeners(AuditingEntityListener.class)
public class Waypoint implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(nullable = true)
    private int time;

    @Column(nullable = true)
    private float latitude;

    @Column(nullable = true)
    private float longitude;

    @Column(nullable = true)
    private float baro_altitude;
    
    @Column(nullable = true)
    private float true_track;

    @Column(nullable = true)
    private boolean on_ground;
    
    @ManyToOne
    @Transient
    private Track track;

    public Waypoint(){
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getBaro_altitude() {
        return baro_altitude;
    }

    public void setBaro_altitude(float baro_altitude) {
        this.baro_altitude = baro_altitude;
    }

    public float getTrue_track() {
        return true_track;
    }

    public void setTrue_track(float true_track) {
        this.true_track = true_track;
    }

    public boolean isOn_ground() {
        return on_ground;
    }

    public void setOn_ground(boolean on_ground) {
        this.on_ground = on_ground;
    }
}

/**
 * 0	time	integer	Time which the given waypoint is associated with in seconds since epoch (Unix time).
 * 1	latitude	float	WGS-84 latitude in decimal degrees. Can be null.
 * 2	longitude	float	WGS-84 longitude in decimal degrees. Can be null.
 * 3	baro_altitude	float	Barometric altitude in meters. Can be null.
 * 4	true_track	float	True track in decimal degrees clockwise from north (north=0°). Can be null.
 * 5	on_ground	boolean	Boolean value which indicates if the position was retrieved from a surface position report.
 */