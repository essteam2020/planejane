package ua.pt.databaseconnector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
  * GET /flights/all
  * needs:
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/aircraft
  * needs:
  *  - icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation. All letters need to be lower case
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/arrival
  * needs:
  *  - airport	string	ICAO identier for the airport
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */

 /**
  * GET /flights/departure
  * needs:
  *  - airport	string	ICAO identier for the airport
  *  - begin	integer	Start of time interval to retrieve flights for as Unix time (seconds since epoch)
  *  - end	integer	End of time interval to retrieve flights for as Unix time (seconds since epoch)
  */
  
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "flights")
@EntityListeners(AuditingEntityListener.class)
public class Flights implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @JoinColumn(name = "flightId")
    @OneToMany(cascade = CascadeType.ALL)
    @OrderColumn
    public List<Flight> flights = new ArrayList<Flight>();

    public Flights(){}

    public Flights(List<Flight> flights){
        this.flights = flights;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

}
