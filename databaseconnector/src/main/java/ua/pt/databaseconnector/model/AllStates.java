package ua.pt.databaseconnector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/*
* GET /states/all
*/
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "allStates")
@EntityListeners(AuditingEntityListener.class)
public class AllStates implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = true)
    private int time;
    
    @JoinColumn(name = "stateVectorId")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn
    private List<StateVector> states = new ArrayList<StateVector>();

    public AllStates() {
    }

    public AllStates(int time, List<StateVector> states){
        this.time = time;
        this.states = states;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<StateVector> getStates() {
        return states;
    }

    public void setStates(List<StateVector> states) {
        this.states = states;
    }

}
