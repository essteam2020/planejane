package ua.pt.databaseconnector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * GET /tracks needs: - icao24 string Unique ICAO 24-bit address of the
 * transponder in hex string representation. All letters need to be lower case -
 * time integer Unix time in seconds since epoch. It can be any time betwee
 * start and end of a known flight. If time = 0, get the live track if there is
 * any flight ongoing for the given aircraft.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "track")
@EntityListeners(AuditingEntityListener.class)
public class Track implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(nullable = true)
    private String icao24;
    
    @JoinColumn(name = "waypointId")
    @OneToMany(cascade = CascadeType.ALL)
    @OrderColumn
    private List<Waypoint> path = new ArrayList<Waypoint>();

    public Track(){
    }

    public Track(String icao24, List<Waypoint> path){
        this.icao24 = icao24;
        this.path = path;
    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public List<Waypoint> getPath() {
        return path;
    }

    public void setPath(List<Waypoint> path) {
        this.path = path;
    }

}

/**
 * icao24	string	Unique ICAO 24-bit address of the transponder in lower case hex string representation.
 * startTime	integer	Time of the first waypoint in seconds since epoch (Unix time).
 * endTime	integer	Time of the last waypoint in seconds since epoch (Unix time).
 * callsign	string	Callsign (8 characters) that holds for the whole track. Can be null.
 * path	array	Waypoints of the trajectory (description below).
 */