package ua.pt.databaseconnector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "stateVector")
@EntityListeners(AuditingEntityListener.class)
public class StateVector implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = true)
    private String icao24;

    @Column(nullable = true)
    private String callsign;

    @Column(nullable = true)
    private String origin_country;

    @Column(nullable = true)
    private int time_position;

    @Column(nullable = true)
    private int last_contact;

    @Column(nullable = true)
    private float longitude;

    @Column(nullable = true)
    private float latitude;

    @Column(nullable = true)
    private float baro_altitude;

    @Column(nullable = true)
    private boolean on_ground;

    @Column(nullable = true)
    private float velocity;

    @Column(nullable = true)
    private float true_track;

    @Column(nullable = true)
    private float vertical_rate;

    @Column(nullable = true)
    private float geo_altitude;

    @Column(nullable = true)
    private String squawk;

    @Column(nullable = true)
    private boolean spi;

    @Column(nullable = true)
    private int position_source;

    @ManyToOne
    @Transient
    private AllStates allStates;

    public StateVector() {

    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getOrigin_country() {
        return origin_country;
    }

    public void setOrigin_country(String origin_country) {
        this.origin_country = origin_country;
    }

    public int getTime_position() {
        return time_position;
    }

    public void setTime_position(int time_position) {
        this.time_position = time_position;
    }

    public int getLast_contact() {
        return last_contact;
    }

    public void setLast_contact(int last_contact) {
        this.last_contact = last_contact;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getBaro_altitude() {
        return baro_altitude;
    }

    public void setBaro_altitude(float baro_altitude) {
        this.baro_altitude = baro_altitude;
    }

    public boolean isOn_ground() {
        return on_ground;
    }

    public void setOn_ground(boolean on_ground) {
        this.on_ground = on_ground;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public float getTrue_track() {
        return true_track;
    }

    public void setTrue_track(float true_track) {
        this.true_track = true_track;
    }

    public float getVertical_rate() {
        return vertical_rate;
    }

    public void setVertical_rate(float vertical_rate) {
        this.vertical_rate = vertical_rate;
    }


    public float getGeo_altitude() {
        return geo_altitude;
    }

    public void setGeo_altitude(float geo_altitude) {
        this.geo_altitude = geo_altitude;
    }

    public String getSquawk() {
        return squawk;
    }

    public void setSquawk(String squawk) {
        this.squawk = squawk;
    }

    public boolean isSpi() {
        return spi;
    }

    public void setSpi(boolean spi) {
        this.spi = spi;
    }

    public int getPosition_source() {
        return position_source;
    }

    public void setPosition_source(int position_source) {
        this.position_source = position_source;
    }

    public AllStates getAllStates() {
        return allStates;
    }

    public void setAllStates(AllStates allStates) {
        this.allStates = allStates;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        StateVector other = (StateVector) obj;
        // if (allStates == null) {
        //     if (other.allStates != null)
        //         return false;
        // } else if (!allStates.equals(other.allStates))
        //     return false;
        if (Float.floatToIntBits(baro_altitude) != Float.floatToIntBits(other.baro_altitude))
            return false;
        if (callsign == null) {
            if (other.callsign != null)
                return false;
        } else if (!callsign.equals(other.callsign))
            return false;
        if (Float.floatToIntBits(geo_altitude) != Float.floatToIntBits(other.geo_altitude))
            return false;
        if (icao24 == null) {
            if (other.icao24 != null)
                return false;
        } else if (!icao24.equals(other.icao24))
            return false;
        // if (id == null) {
        //     if (other.id != null)
        //         return false;
        // } else if (!id.equals(other.id))
        //     return false;
        if (last_contact != other.last_contact)
            return false;
        if (Float.floatToIntBits(latitude) != Float.floatToIntBits(other.latitude))
            return false;
        if (Float.floatToIntBits(longitude) != Float.floatToIntBits(other.longitude))
            return false;
        if (on_ground != other.on_ground)
            return false;
        if (origin_country == null) {
            if (other.origin_country != null)
                return false;
        } else if (!origin_country.equals(other.origin_country))
            return false;
        if (position_source != other.position_source)
            return false;
        if (spi != other.spi)
            return false;
        if (squawk == null) {
            if (other.squawk != null)
                return false;
        } else if (!squawk.equals(other.squawk))
            return false;
        if (time_position != other.time_position)
            return false;
        if (Float.floatToIntBits(true_track) != Float.floatToIntBits(other.true_track))
            return false;
        if (Float.floatToIntBits(velocity) != Float.floatToIntBits(other.velocity))
            return false;
        if (Float.floatToIntBits(vertical_rate) != Float.floatToIntBits(other.vertical_rate))
            return false;
        return true;
    }

}


/*
0	icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation.
1	callsign	string	Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
2	origin_country	string	Country name inferred from the ICAO 24-bit address.
3	time_position	int	Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
4	last_contact	int	Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
5	longitude	float	WGS-84 longitude in decimal degrees. Can be null.
6	latitude	float	WGS-84 latitude in decimal degrees. Can be null.
7	baro_altitude	float	Barometric altitude in meters. Can be null.
8	on_ground	boolean	Boolean value which indicates if the position was retrieved from a surface position report.
9	velocity	float	Velocity over ground in m/s. Can be null.
10	true_track	float	True track in decimal degrees clockwise from north (north=0°). Can be null.
11	vertical_rate	float	Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
13	geo_altitude	float	Geometric altitude in meters. Can be null.
14	squawk	string	The transponder code aka Squawk. Can be null.
15	spi	boolean	Whether flight status indicates special purpose indicator.
16	position_source	int	Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
*/
