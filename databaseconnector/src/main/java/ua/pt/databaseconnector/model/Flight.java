package ua.pt.databaseconnector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Flight
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "flight")
@EntityListeners(AuditingEntityListener.class)
public class Flight implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = true)
    private String icao24;

    @Column(nullable = true)
    private int firstSeen;

    @Column(nullable = true)
    private String estDepartureAirport;

    @Column(nullable = true)
    private int lastSeen;

    @Column(nullable = true)
    private String estArrivalAirport;

    @Column(nullable = true)
    private String callSign;

    @Column(nullable = true)
    private int estDepartureAirportHorizDistance;

    @Column(nullable = true)
    private int estDepartureAirportVertDistance;

    @Column(nullable = true)
    private int estArrivalAirportHorizDistance;

    @Column(nullable = true)
    private int estArrivalAirportVertDistance;

    @Column(nullable = true)
    private int departureAirportCandidatesCount;

    @Column(nullable = true)
    private int arrivalAirportCandidatesCount;
    
    @ManyToOne
    @Transient
    private Flights flights;


    public Flight(){
    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public int getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(int firstSeen) {
        this.firstSeen = firstSeen;
    }

    public String getEstDepartureAirport() {
        return estDepartureAirport;
    }

    public void setEstDepartureAirport(String estDepartureAirport) {
        this.estDepartureAirport = estDepartureAirport;
    }

    public int getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(int lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getEstArrivalAirport() {
        return estArrivalAirport;
    }

    public void setEstArrivalAirport(String estArrivalAirport) {
        this.estArrivalAirport = estArrivalAirport;
    }

    public String getCallSign() {
        return callSign;
    }

    public void setCallSign(String callSign) {
        this.callSign = callSign;
    }

    public int getEstDepartureAirportHorizDistance() {
        return estDepartureAirportHorizDistance;
    }

    public void setEstDepartureAirportHorizDistance(int estDepartureAirportHorizDistance) {
        this.estDepartureAirportHorizDistance = estDepartureAirportHorizDistance;
    }

    public int getEstDepartureAirportVertDistance() {
        return estDepartureAirportVertDistance;
    }

    public void setEstDepartureAirportVertDistance(int estDepartureAirportVertDistance) {
        this.estDepartureAirportVertDistance = estDepartureAirportVertDistance;
    }

    public int getEstArrivalAirportHorizDistance() {
        return estArrivalAirportHorizDistance;
    }

    public void setEstArrivalAirportHorizDistance(int estArrivalAirportHorizDistance) {
        this.estArrivalAirportHorizDistance = estArrivalAirportHorizDistance;
    }

    public int getEstArrivalAirportVertDistance() {
        return estArrivalAirportVertDistance;
    }

    public void setEstArrivalAirportVertDistance(int estArrivalAirportVertDistance) {
        this.estArrivalAirportVertDistance = estArrivalAirportVertDistance;
    }

    public int getDepartureAirportCandidatesCount() {
        return departureAirportCandidatesCount;
    }

    public void setDepartureAirportCandidatesCount(int departureAirportCandidatesCount) {
        this.departureAirportCandidatesCount = departureAirportCandidatesCount;
    }

    public int getArrivalAirportCandidatesCount() {
        return arrivalAirportCandidatesCount;
    }

    public void setArrivalAirportCandidatesCount(int arrivalAirportCandidatesCount) {
        this.arrivalAirportCandidatesCount = arrivalAirportCandidatesCount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + arrivalAirportCandidatesCount;
        result = prime * result + ((callSign == null) ? 0 : callSign.hashCode());
        result = prime * result + departureAirportCandidatesCount;
        result = prime * result + ((estArrivalAirport == null) ? 0 : estArrivalAirport.hashCode());
        result = prime * result + estArrivalAirportHorizDistance;
        result = prime * result + estArrivalAirportVertDistance;
        result = prime * result + ((estDepartureAirport == null) ? 0 : estDepartureAirport.hashCode());
        result = prime * result + estDepartureAirportHorizDistance;
        result = prime * result + estDepartureAirportVertDistance;
        result = prime * result + firstSeen;
        result = prime * result + ((icao24 == null) ? 0 : icao24.hashCode());
        result = prime * result + lastSeen;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Flight other = (Flight) obj;
        if (arrivalAirportCandidatesCount != other.arrivalAirportCandidatesCount)
            return false;
        if (callSign == null) {
            if (other.callSign != null)
                return false;
        } else if (!callSign.equals(other.callSign))
            return false;
        if (departureAirportCandidatesCount != other.departureAirportCandidatesCount)
            return false;
        if (estArrivalAirport == null) {
            if (other.estArrivalAirport != null)
                return false;
        } else if (!estArrivalAirport.equals(other.estArrivalAirport))
            return false;
        if (estArrivalAirportHorizDistance != other.estArrivalAirportHorizDistance)
            return false;
        if (estArrivalAirportVertDistance != other.estArrivalAirportVertDistance)
            return false;
        if (estDepartureAirport == null) {
            if (other.estDepartureAirport != null)
                return false;
        } else if (!estDepartureAirport.equals(other.estDepartureAirport))
            return false;
        if (estDepartureAirportHorizDistance != other.estDepartureAirportHorizDistance)
            return false;
        if (estDepartureAirportVertDistance != other.estDepartureAirportVertDistance)
            return false;
        if (firstSeen != other.firstSeen)
            return false;
        if (icao24 == null) {
            if (other.icao24 != null)
                return false;
        } else if (!icao24.equals(other.icao24))
            return false;
        if (lastSeen != other.lastSeen)
            return false;
        return true;
    }

}




/*
icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation. All letters are lower case.
firstSeen	integer	Estimated time of departure for the flight as Unix time (seconds since epoch).
estDepartureAirport	string	ICAO code of the estimated departure airport. Can be null if the airport could not be identified.
lastSeen	integer	Estimated time of arrival for the flight as Unix time (seconds since epoch)
estArrivalAirport	string	ICAO code of the estimated arrival airport. Can be null if the airport could not be identified.
callsign	string	Callsign of the vehicle (8 chars). Can be null if no callsign has been received. If the vehicle transmits multiple callsigns during the flight, we take the one seen most frequently
estDepartureAirportHorizDistance	integer	Horizontal distance of the last received airborne position to the estimated departure airport in meters
estDepartureAirportVertDistance	integer	Vertical distance of the last received airborne position to the estimated departure airport in meters
estArrivalAirportHorizDistance	integer	Horizontal distance of the last received airborne position to the estimated arrival airport in meters
estArrivalAirportVertDistance	integer	Vertical distance of the last received airborne position to the estimated arrival airport in meters
departureAirportCandidatesCount	integer	Number of other possible departure airports. These are airports in short distance to estDepartureAirport.
arrivalAirportCandidatesCount	integer	Number of other possible departure airports. These are airports in short distance to estArrivalAirport.
*/