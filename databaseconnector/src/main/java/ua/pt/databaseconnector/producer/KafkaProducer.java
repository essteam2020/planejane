package ua.pt.databaseconnector.producer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

public class KafkaProducer {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    public void send(String topic, String key, String nice) {
        // kafkaTemplate.send(topic, nice);
        kafkaTemplate.send(topic, key, nice);
    }

    public void send(String topic, String nice) {
        // kafkaTemplate.send(topic, nice);
        kafkaTemplate.send(topic, nice);
    }
}
