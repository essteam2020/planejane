package ua.pt.databaseconnector.serializers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.TextNode;

import org.springframework.boot.jackson.JsonComponent;

import ua.pt.databaseconnector.model.Track;
import ua.pt.databaseconnector.model.Waypoint;

/**
 * TrackDeserializer
 */
@JsonComponent
public class TrackCombinedSerializer {
    
    /**
     * TrackSerializer
     */
    public static class TrackSerializer extends JsonSerializer<Track> {
    

        @Override
        public void serialize(Track value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartObject();
            gen.writeStringField("icao24", value.getIcao24());
            gen.writeArrayFieldStart("path");
            for(Waypoint wp : value.getPath()){
                gen.writeObject(wp);
            }
            gen.writeEndArray();
            gen.writeEndObject();
        }

    }
    
    /**
     * TrackDeserializer
     */
    public static class TrackDeserializer extends JsonDeserializer<Track>{

        @Override
        public Track deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {

            TreeNode treeNode = p.getCodec().readTree(p);

            String icao24 = ((TextNode)treeNode.get("icao24")).asText();

            ArrayNode path = (ArrayNode) treeNode.get("path");
            List<Waypoint> waypointList = new ArrayList<>();

            for(int i=0; i<path.size(); i++)
            {
                ArrayNode wp = (ArrayNode) path.get(i);
                
                Waypoint waypoint = new Waypoint();
                waypoint.setTime(wp.get(0).asInt());
                waypoint.setLatitude((float) wp.get(1).asDouble());
                waypoint.setLongitude((float) wp.get(2).asDouble());
                waypoint.setBaro_altitude((float) wp.get(3).asDouble());
                waypoint.setTrue_track((float) wp.get(4).asDouble());
                waypoint.setOn_ground(wp.get(5).asBoolean());
                waypointList.add(waypoint);
            }

            return new Track(icao24, waypointList);
        }
    
    }
    
}
