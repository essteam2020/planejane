package ua.pt.databaseconnector.serializers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;

import org.springframework.boot.jackson.JsonComponent;

import ua.pt.databaseconnector.model.AllStates;
import ua.pt.databaseconnector.model.StateVector;

/**
 * AllStatesDeserializer
 */
@JsonComponent
public class AllStatesCombinedSerializer {
    
    /**
     * AllStatesSerializer
     */
    public static class AllStatesSerializer extends JsonSerializer<AllStates> {
    
        // ObjectMapper omp = new ObjectMapper();

        @Override
        public void serialize(AllStates value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartObject();
            gen.writeNumberField("time", value.getTime());
            gen.writeArrayFieldStart("states");
            for(StateVector sv : value.getStates()){
                gen.writeObject(sv);
                // gen.writeString(omp.writeValueAsString(sv));
            }
            gen.writeEndArray();
            gen.writeEndObject();
        }

    }
    
    /**
     * AllStatesDeserializer
     */
    public static class AllStatesDeserializer extends JsonDeserializer<AllStates>{

        @Override
        public AllStates deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {

            TreeNode treeNode = p.getCodec().readTree(p);
            Integer time = ((IntNode) treeNode.get("time")).asInt();
            ArrayNode states = (ArrayNode) treeNode.get("states");
            List<StateVector> svs = new ArrayList<>();
            
            for(int i=0; i<states.size(); i++) {
                ArrayNode state = (ArrayNode) states.get(i);
                StateVector stateVector = new StateVector();
                
                stateVector.setIcao24(state.get(0).asText());
                stateVector.setCallsign(state.get(1).asText());
                stateVector.setOrigin_country(state.get(2).asText());
                stateVector.setTime_position(state.get(3).asInt());
                stateVector.setLast_contact(state.get(4).asInt());
                stateVector.setLongitude((float) state.get(5).asDouble());
                stateVector.setLatitude((float) state.get(6).asDouble());
                stateVector.setBaro_altitude((float) state.get(7).asDouble());
                stateVector.setOn_ground(state.get(8).asBoolean());
                stateVector.setVelocity((float) state.get(9).asDouble());
                stateVector.setTrue_track((float) state.get(10).asDouble());
                stateVector.setVertical_rate((float) state.get(11).asDouble());          
                stateVector.setGeo_altitude((float) state.get(13).asDouble());
                stateVector.setSquawk(state.get(14).asText());
                stateVector.setSpi(state.get(15).asBoolean());
                stateVector.setPosition_source(state.get(16).asInt());
                
                svs.add(stateVector);
            }

            return new AllStates(time, svs);
        }
    
    }
    
}

