package ua.pt.databaseconnector.serializers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.boot.jackson.JsonComponent;

import ua.pt.databaseconnector.model.Flight;
import ua.pt.databaseconnector.model.Flights;

/**
 * FlightsDeserializer
 */
@JsonComponent
public class FlightsCombinedSerializer {

    private static final Logger logger = LoggerFactory.getLogger(FlightsCombinedSerializer.class);

    
    /**
     * FlightsSerializer
     */
    public static class FlightsSerializer extends JsonSerializer<Flights> {
        @Override
        public void serialize(Flights value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartObject();
            gen.writeArrayFieldStart("flights");
            for(Flight sv : value.getFlights()){
                gen.writeObject(sv);
            }
            gen.writeEndArray();
            gen.writeEndObject();
        }

    }
    
    /**
     * AllStatesDeserializer
     */
    public static class FlightsDeserializer extends JsonDeserializer<Flights>{
        
        ObjectMapper omp = new ObjectMapper();
        
        @Override
        public Flights deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {

            String json = p.readValueAsTree().toString();
            List<Flight> fls = Arrays.asList(omp.readValue(json, Flight[].class));
            return new Flights(fls);
        }
    
    }
    
}

