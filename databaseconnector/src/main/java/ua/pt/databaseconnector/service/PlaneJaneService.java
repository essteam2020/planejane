package ua.pt.databaseconnector.service;

import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ua.pt.databaseconnector.model.AllStates;
import ua.pt.databaseconnector.model.Flights;
import ua.pt.databaseconnector.model.Track;

/**
 * PlaneJaneService
 */
@Service
public class PlaneJaneService {

    private static final String OPENSKY_API = "https://planejane:plane&jane@opensky-network.org/api";
    private static final Logger logger = LoggerFactory.getLogger(PlaneJaneService.class);

    @Autowired
    private RestTemplate restTemplate;

    public PlaneJaneService() {
    }

    public AllStates getAllStates() {
        logger.info("Requesting all states!");
        try {
            URI url = new URI(OPENSKY_API + "/states/all");
            return invoke(url, AllStates.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public AllStates getAllStates(int time, String icao24) {
        String url_short = "?time="+time+"&icao24="+icao24;
        logger.info("Requesting states with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/states/all" + url_short);
            return invoke(url, AllStates.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
        lamin	float	lower bound for the latitude in decimal degrees
        lomin	float	lower bound for the longitude in decimal degrees
        lamax	float	upper bound for the latitude in decimal degrees
        lomax	float	upper bound for the longitude in decimal degrees
    */
    public AllStates getAllStates(int time, String icao24, float lamin, float lomin, float lamax, float lomax) {
        String url_short = "?time="+time+"&icao24="+icao24+"&lamin="+lamin+"&lomin"+lomin+"&lamax"+lamax+"&lomax"+lomax;

        logger.info("Requesting states with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/states/all" + url_short);
            return invoke(url, AllStates.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Flights getFlightsWithTime(long begin, long end){
        String url_short = "?begin="+begin+"&end="+end;
        logger.info("Requesting flights with time with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/flights/all" + url_short);
            return invoke(url, Flights.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Flights getFlightsByAircraft(String icao24, int begin, int end){
        String url_short = "?icao24="+icao24+"&begin="+begin+"&end="+end;
        logger.info("Requesting flights by aircraft with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/flights/aircraft" + url_short);
            return invoke(url, Flights.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Flights getFlightsByArrival(String airport, int begin, int end){
        String url_short = "?airport="+airport+"&begin="+begin+"&end="+end;
        logger.info("Requesting flights by Arrival Airport with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/flights/arrival" + url_short);
            return invoke(url, Flights.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Flights getFlightsByDeparture(String airport, int begin, int end){
        String url_short = "?airport="+airport+"&begin="+begin+"&end="+end;
        logger.info("Requesting flights by Departure Airport with url: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/flights/departure" + url_short);
            return invoke(url, Flights.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Track getTrackByAircraft(String icao24, int time){
        // "https://USERNAME:PASSWORD@opensky-network.org/api/tracks/all?icao24=3c4b26&time=0"
        String url_short = "?icao24="+icao24+"&time="+time;
        logger.info("Requesting tracking by Aircraft: {}", url_short);
        try {
            URI url = new URI(OPENSKY_API + "/tracks/all" + url_short);
            return invoke(url, Track.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }


    private <T> T invoke(URI url, Class<T> responseType) {
        System.out.println("\n\nurl:"+url+"\n\n");
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<T> exchange = this.restTemplate
                .exchange(request, responseType);
        return exchange.getBody();
    }
    
}