pipeline {

    agent any

    environment {
        database_connector = ""
        realtime_processing = ""
        statistics_processing = ""
        planejane_aggregator = ""
    }

    parameters {
        booleanParam(
            name: 'test',
            defaultValue: true,
            description: 'Test Projects'
        )
        booleanParam(
            name: 'build',
            defaultValue: true,
            description: 'Build Projects'
        )
        booleanParam(
            name: 'dockercreate',
            defaultValue: true,
            description: 'Create docker image'
        )
        booleanParam(
            name: 'pushregistry',
            defaultValue: true,
            description: 'Push image to registry'
        )
        booleanParam(
            name: 'deploy',
            defaultValue: true,
            description: 'Deploy over SSH'
        )
    }

    stages {
        stage('Test') {
            when {
                expression { params.test }
            }
            parallel{
                stage('Database Connector'){
                    steps {
                        dir('databaseconnector'){
                            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                                sh 'mvn test'
                            }
                        }
                    }
                }
                stage('Real Time Processing'){
                    steps {
                        dir('realtimeprocessing'){
                            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                                sh 'mvn test'
                            }
                        }
                    }
                }
                stage('Statistics Processing'){
                    steps {
                        dir('statisticsprocessing'){
                            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                                sh 'mvn test'
                            }
                        }
                    }
                }
                stage('Planejane Aggregator'){
                    steps {
                        dir('planejaneaggregator'){
                            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                                sh 'mvn test'
                            }
                        }
                    }
                }
            }
        }
        stage('Build') {
            when {
                expression { params.build }
            }
            parallel{
                stage('Database Connector'){
                    steps {
                        dir('databaseconnector'){
                            sh 'mvn clean install -DskipTests'
                        }
                    }
                }
                stage('Real Time Processing'){
                    steps {
                        dir('realtimeprocessing'){
                            sh 'mvn clean install -DskipTests'
                        }
                    }
                }
                stage('Statistics Processing'){
                    steps {
                        dir('statisticsprocessing'){
                            sh 'mvn clean install -DskipTests'
                        }
                    }
                }
                stage('Planejane Aggregator'){
                    steps {
                        dir('planejaneaggregator'){
                            sh 'mvn clean install -DskipTests'
                        }
                    }
                }
                
            }
        }
        stage('DockerCreate') {
            when {
                expression { params.dockercreate }
            }
            parallel{
                stage('Database Connector'){
                    steps {
                        dir('databaseconnector'){
                            script {
                                database_connector = docker.build("esp55/databaseconnector")
                            }
                        }
                    }
                }
                stage('Real Time Processing'){
                    steps {
                        dir('realtimeprocessing'){
                            script {
                                realtime_processing = docker.build("esp55/realtimeprocessing")
                            }
                        }
                    }
                }
                stage('Statistics Processing'){
                    steps {
                        dir('statisticsprocessing'){
                            script {
                                statistics_processing = docker.build("esp55/statisticsprocessing")
                            }
                        }
                    }
                }
                stage('Planejane Aggregator'){
                    steps {
                        dir('planejaneaggregator'){
                            script {
                                planejane_aggregator = docker.build("esp55/planejaneaggregator")
                            }
                        }
                    }
                }
            }
        }

        stage('DockerImagePush') {
            when {
                expression { params.pushregistry }
            }
            steps {
                script {
                    docker.withRegistry('http://192.168.160.99:5000') {
                        database_connector.push("${env.BUILD_NUMBER}")
                        database_connector.push("latest")
                    }
                    docker.withRegistry('http://192.168.160.99:5000') {
                        realtime_processing.push("${env.BUILD_NUMBER}")
                        realtime_processing.push("latest")
                    }
                    docker.withRegistry('http://192.168.160.99:5000') {
                        statistics_processing.push("${env.BUILD_NUMBER}")
                        statistics_processing.push("latest")
                    }
                    docker.withRegistry('http://192.168.160.99:5000') {
                        planejane_aggregator.push("${env.BUILD_NUMBER}")
                        planejane_aggregator.push("latest")
                    }
                }
            }
        }

        stage('Deploy') {
            when {
                expression { params.deploy }
            }
            steps {
                sshagent (credentials: ['p55_planejane_registry']) {
                    sh 'ssh -o StrictHostKeyChecking=no -l esp55 192.168.160.20 -a "docker stop esp55_databaseconnector && docker rm esp55_databaseconnector && docker rmi 192.168.160.99:5000/esp55/databaseconnector && docker pull 192.168.160.99:5000/esp55/databaseconnector && docker create -p 55070:55070 --name esp55_databaseconnector --log-opt max-size=10m --log-opt max-file=5 192.168.160.99:5000/esp55/databaseconnector && docker start esp55_databaseconnector"'
                    sleep 600
                    sh 'ssh -o StrictHostKeyChecking=no -l esp55 192.168.160.20 -a "docker stop esp55_realtimeprocessing && docker rm esp55_realtimeprocessing && docker rmi 192.168.160.99:5000/esp55/realtimeprocessing && docker pull 192.168.160.99:5000/esp55/realtimeprocessing && docker create --name esp55_realtimeprocessing --log-opt max-size=10m --log-opt max-file=5 192.168.160.99:5000/esp55/realtimeprocessing && docker start esp55_realtimeprocessing"'
                    sh 'ssh -o StrictHostKeyChecking=no -l esp55 192.168.160.20 -a "docker stop esp55_statisticsprocessing && docker rm esp55_statisticsprocessing && docker rmi 192.168.160.99:5000/esp55/statisticsprocessing && docker pull 192.168.160.99:5000/esp55/statisticsprocessing && docker create --name esp55_statisticsprocessing --log-opt max-size=10m --log-opt max-file=5 192.168.160.99:5000/esp55/statisticsprocessing && docker start esp55_statisticsprocessing"'
                    sleep 120
                    sh 'ssh -o StrictHostKeyChecking=no -l esp55 192.168.160.20 -a "docker stop esp55_planejaneaggregator && docker rm esp55_planejaneaggregator && docker rmi 192.168.160.99:5000/esp55/planejaneaggregator && docker pull 192.168.160.99:5000/esp55/planejaneaggregator && docker create -p 55080:55080 --name esp55_planejaneaggregator --log-opt max-size=10m --log-opt max-file=5 192.168.160.99:5000/esp55/planejaneaggregator && docker start esp55_planejaneaggregator"'
                }
            }
        }
    }
}
